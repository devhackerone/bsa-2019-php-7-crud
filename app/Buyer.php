<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = 'buyers';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'surname', 'country', 'city', 'addressLine', 'phone'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
