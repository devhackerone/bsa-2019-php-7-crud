<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;
use App\Product;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(1, 50),
        'discount' => $faker->numberBetween(0, 30),
        'product_id' => function () {
            do {
                $product = Product::find(rand(1, 100));
            } while (!$product);

            return $product->id;
        },
        'price' => function (array $order) {
            return Product::find($order['product_id'])
                ->price;
        },
        'sum' => function (array $order) {
            if ($order['discount'] > 0) {
                return round(ceil($order['price'] * (1 - $order['discount'] / 100)) * $order['quantity']);
            }

            return round($order['price'] * $order['quantity']);
        }
    ];
});
