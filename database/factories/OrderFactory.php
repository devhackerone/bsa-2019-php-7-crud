<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'date' => $faker->dateTime(),
        'buyer_id' => $faker->numberBetween(1, 10)
    ];
});
