<?php

use Illuminate\Database\Seeder;
use App\{
    Order, Seller, OrderItem, Product, Buyer
};

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyer::class, 10)->create()
        ->each(function ($buyer){
            $buyer->orders()->saveMany(
                factory(Order::class, 20)->create(['buyer_id' => $buyer->id])
                    ->each(function ($order) {
                    $order->orderItems()->saveMany(
                        factory(OrderItem::class, rand(1, 10))->make()
                    );
                })
            );
        });
    }
}
